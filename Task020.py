def two_even(lst):
    count = 0
    for elem in lst:
        if elem % 2 == 0:
            count += 1
        if count == 2:
            return True
    return False
