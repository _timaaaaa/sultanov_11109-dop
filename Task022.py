def is_there_palindrome(lst):
    for string in lst:
        is_it = True
        for i in range(len(string)//2):
            if string[i] != string[-1-i]:
                is_it = False
        if is_it:
            return is_it
    return False
