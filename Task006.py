from abc import ABC, abstractmethod
import random


class MedicineDevice(ABC):
    """ Класс, описывающий медицинский прибор """
    def __init__(self, name):
        self.name = name

    @abstractmethod
    def get_result(self):
        pass

    @abstractmethod
    def is_good_result(self) -> bool:
        pass

    def __str__(self):
        if self.is_good_result():
            return '%s show the good result' % self.name
        else:
            return '%s show the bad result' % self.name


class HearthDevice(MedicineDevice):
    """ Прибор для оценки сердца (пульса) """
    def __init__(self):
        super().__init__("heart_device")

    def is_good_result(self) -> bool:
        return 60 < self.get_result() < 140

    def get_result(self):
        """ Имитирует получение пульса """
        return random.randint(40, 200)


class HearthDevice2(HearthDevice):
    """
    Тоже прибор для оценки пульса, но другой
    Здесь показано отношение родитель-наследник, это позволяет не реализовывать все методы
    """
    def get_result(self):
        """ Имитирует получение пульса, но другим методом """
        return random.randint(50, 190)


class BloodDevice(MedicineDevice):
    def __init__(self):
        super().__init__("blood_device")

    def is_good_result(self) -> bool:
        result = self.get_result()
        return result[0] > 0.5 and 0.1 < result[1] < 0.9

    def get_result(self):
        """ Возвращает значения крови (например гемоглобин и сахар) """
        return random.random(), random.random()


class ProteinsDevice:
    """ Для сравнения - класс без наследования, приходиться все методы реализовывать с нуля """
    def get_result(self):
        """ Этот метод можно сделать статическим """
        return random.random()

    def is_good_result(self) -> bool:
        return 0.1 < self.get_result() < 0.5

    def __str__(self):
        if self.is_good_result():
            return 'proteins_device show the good result'
        else:
            return 'proteins_device show the bad result'