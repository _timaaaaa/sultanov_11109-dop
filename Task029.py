def myrange(x, y, step=1):
    while x != y:
        yield x
        x += step